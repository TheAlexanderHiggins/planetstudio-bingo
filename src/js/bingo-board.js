import {phrases} from './bingo-phrases'
import _ from 'lodash';

document.addEventListener('DOMContentLoaded', onLoad, false);

function onLoad() {
    newBoard();
    preloadImages();
    document.getElementById("new-card-button").addEventListener('click',newBoard);
    document.getElementById("clear-button").addEventListener('click',clearBoard);
}

function preloadImages(){
    let images = ["./Purple-Chip.png"];
    images.forEach(img => {
        let i = document.createElement('img').setAttribute('src',img);
    })
}
function deleteNodes(nodes){
    while(nodes[0]){
        nodes[0].parentNode.removeChild(nodes[0]);
    }
}

function clearBoard(){
    let chips = document.getElementsByClassName('bingo-chip');
    deleteNodes(chips);
}
const winConditions = [
    //Rows
    [ 0, 1, 2, 3, 4],
    [ 5, 6, 7, 8, 9],
    [10,11,12,13,14],
    [15,16,17,18,19],
    [20,21,22,23,24],

    //Columns
    [ 0, 5,10,15,20],
    [ 1, 6,11,16,21],
    [ 2, 7,12,17,22],
    [ 3, 8,13,18,23],
    [ 4, 9,14,19,24],

    //Diagonals
    [ 0, 6,12,18,24],
    [ 4, 8,12,16,20]

    //Potential others, corners, etc
];

function isWin(){
    let tiles = document.getElementsByClassName('bingo-tile');
    let tokens = [];
    for(let i = 0; i < tiles.length;i++){
        if(i == 12){
            tokens.push(true);
            continue;
        }
        let tile = tiles.item(i);
        let chip = tile.getElementsByClassName('bingo-chip')[0];
        tokens.push(chip && true);
    }

    let win = winConditions.reduce((acc,condition) => {
        let win = condition.reduce((acc,index) => {
            return acc && tokens[index];
        },true);
        return acc || win;
    },false);
    return win;
}

function newBoard(){
    //Clear board
    let tiles = document.getElementsByClassName('bingo-tile');
    deleteNodes(tiles);

    //Add new tiles
    let bingoCard = document.getElementById("bingo-tile-container");
    let usedPhrases = _.shuffle(phrases).slice(0,25);

    usedPhrases[12] = "Free Space!";
    usedPhrases.forEach((phrase,index) => {
        let div = document.createElement("div");
        div.classList.add('bingo-tile');
        if(index == 12)
            div.innerHTML = `<img id="free-space" src="./FreeSpace.svg">`
        else
            div.innerHTML = phrase;
        div.addEventListener('click',() => {
            let chip = div.getElementsByClassName('bingo-chip').item(0);
            if(!chip){
                let img = document.createElement("img");
                img.classList.add('bingo-chip');
                img.setAttribute("src","./Purple-Chip.png");
                div.appendChild(img);
                console.log(isWin());
            }else{
                chip.parentNode.removeChild(chip);
            }
        });
        bingoCard.appendChild(div);
    });
}

function placeChip(e){
    console.log(e)
}